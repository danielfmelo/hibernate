import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class TestDAO {

    EntityManagerFactory factory = null;
    EntityManager entityManager = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getEntityManager();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Bank> getBanks() {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Bank";
            List<Bank> cs = (List<Bank>)entityManager.createQuery(sql).getResultList();
            entityManager.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }

    }

    /** Used to get a single customer from database */
    public Bank getCustomer(int id) {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Bank where id=" + Integer.toString(id);
            Bank c = (Bank)entityManager.createQuery(sql).getSingleResult();
            entityManager.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }
    }

    public boolean addBank(Bank bank) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean removeBank(int id) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Bank bank = entityManager.find(Bank.class, id);
            entityManager.remove(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updateBank(int id, String name) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Bank bank = entityManager.find(Bank.class, id);
            bank.setName(name);
            entityManager.merge(bank);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

}
