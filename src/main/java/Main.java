import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        options();
    }

    public static void options() {
        int ctn = 1;
        TestDAO t = TestDAO.getInstance();

        System.out.println("What do you want to do?");
        System.out.println("1- Add a bank");
        System.out.println("2- List banks available");
        System.out.println("3- Update bank name");
        System.out.println("4- Remove a bank");
        System.out.println("0- Exit");

        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        switch (num){

            case 1:
                addBank(t);
                break;
            case 2:
                listBanks(t);
                break;
            case 3:
                updateBank(t);
                break;
            case 4:
                removeBank(t);
                break;
            case 0:
                ctn = 0;
                break;
        }

        if(ctn == 1){
            options();
        }else{
            System.out.println("Exiting...");
            System.exit(1);
        }
    }

    public static void addBank(TestDAO t) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type the name of the bank:");
        String name = input.nextLine();
        Bank bank = new Bank();
        bank.setName(name);
        if(t.addBank(bank)){
            listBanks(t);
        }else{
            System.out.println("Bank not added");
        }
    }

    public static void listBanks(TestDAO t) {
        System.out.println("===================== Banks Available =====================");
        for (Bank b: t.getBanks()) {
            System.out.println(b.getId() + " - " + b.getName());
        }
    }

    public static void updateBank(TestDAO t) {
        System.out.println("===================== Banks Available =====================");
        for (Bank b: t.getBanks()) {
            System.out.println(b.getId() + " - " + b.getName());
        }
        Scanner input2 = new Scanner(System.in);
        Scanner input4 = new Scanner(System.in);
        System.out.println("Type the ID of the bank:");
        int id = input2.nextInt();
        System.out.println("Type the new name of the bank:");
        String newName = input4.nextLine();
        if(t.updateBank(id, newName)){
            listBanks(t);
        }else{
            System.out.println("Bank not updated");
        }
    }

    public static void removeBank(TestDAO t) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type the ID of the bank you want to remove:");
        int id = input.nextInt();
        if(t.removeBank(id)){
            listBanks(t);
        }else{
            System.out.println("Bank not updated");
        }
    }
}
